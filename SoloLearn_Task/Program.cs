﻿using System;

namespace SoloLearn_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            int digit = 0,
                sum = 0;
            Console.WriteLine("\t\t\t\tResult:");
            Console.WriteLine("Harshad Numbers:");
            for (int i = 1; i < 100; i++)
            {
                digit = i;
                sum = 0;
                while (digit > 0)
                {
                    sum +=digit%10;
                    digit /= 10;
                }
                if(i%sum==0)
                    Console.WriteLine($"<{i}>");
            }
            Console.ReadKey();
        }
    }
}
